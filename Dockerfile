FROM php:7.1.1
LABEL maintainer Darwin "darwin@buonzz.com"
RUN apt-get update -y
RUN curl -sL https://deb.nodesource.com/setup_7.x | bash -
RUN apt-get install -y git nodejs wget libcurl4-gnutls-dev mysql-client libicu-dev libmcrypt-dev libvpx-dev libjpeg-dev libpng-dev libxpm-dev zlib1g-dev libfreetype6-dev libxml2-dev libexpat1-dev libbz2-dev libgmp3-dev libldap2-dev unixodbc-dev libpq-dev libsqlite3-dev libaspell-dev libsnmp-dev libpcre3-dev libtidy-dev -yqq
RUN docker-php-ext-install mbstring mcrypt pdo_mysql curl json intl gd xml zip bz2 opcache
RUN wget --no-check-certificate https://xdebug.org/files/xdebug-2.5.0.tgz
RUN tar xzf xdebug-2.5.0.tgz && \ 
	rm xdebug-2.5.0.tgz && \
	cd xdebug-2.5.0 && \ 
	phpize && \
	./configure --enable-xdebug && \ 
	make && \
	cp modules/xdebug.so /usr/lib/. && \
	echo 'zend_extension="/usr/lib/xdebug.so"' > /usr/local/etc/php/php.ini && \ 
	echo 'xdebug.remote_enable=1' >> /usr/local/etc/php/php.ini

# Install composer and add its bin to the PATH.
RUN curl -s http://getcomposer.org/installer | php && \
    mv composer.phar /usr/local/bin/composer


CMD ["tail", "-f", "/var/log/php_errors.log"]