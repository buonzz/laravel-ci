# Laravel CI

Gitlab CI Docker based executor image for Laravel


## Usage

- clone this repo to the Docker executor
- run the ./build.sh script

or run this image directly

```
docker run -it buonzz/laravel-ci /bin/bash
```
edit the /etc/gitlab-runner/config.toml
add the following config
```
[runners.docker]
pull_policy = "if-not-present"
```
restart
gitlab-ci-multi-runner stop
gitlab-ci-multi-runner start


In your .gitlab-ci.yml, set the the image to point to that base image

```
image: buonzz/laravel-ci:latest
```